package Actors

object caseClasses{
  case class callParentActor(callParent : String)
  case class callChildActor(callChild: String)
  case class callOtherChildActor(callOtherChild: String)
}
