package Actors

import Actors.caseClasses.{callChildActor, callOtherChildActor, callParentActor}
import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, ActorSystem, OneForOneStrategy, PoisonPill, Props}

object akkaActors {

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("akkActorSystem")
    val parentActor = system.actorOf(Props[parentActor],"parentActor")

    parentActor ! callParentActor("callParent")
    parentActor ! PoisonPill

    system.terminate()
  }



  class parentActor extends Actor {

    def receive ={
      case callParentActor("callParent") =>
        println("Parent actor is beign called")
        context.actorOf(Props[childActor],"childActor") ! callChildActor("callChild")
    }

    override def postStop(): Unit ={
      println("Post stop is beign called")
      super.postStop()
    }

    override val supervisorStrategy = OneForOneStrategy(loggingEnabled = true) {
      case _: ArithmeticException      ⇒ Resume
      case _: NullPointerException     ⇒ Restart
      case _: IllegalArgumentException ⇒ Stop
      case _: Exception                ⇒ Escalate

    }

  }

  class childActor() extends Actor{

    def receive={

      case callChildActor("callChild") =>
        println("Child actor is beign called")
        context.actorOf(Props[childActor],"childActor") ! callOtherChildActor("callOtherChild")

      case callOtherChildActor("callOtherChild") =>
        println("Hi there, I'm other child actor")
    }

  }
}
