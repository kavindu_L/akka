package AkkaAndSpark

import akka.actor.{Actor, ActorSystem, Props}
import org.apache.spark.sql.SparkSession

class sparkActor extends Actor {

  def receive = {
    case "filter data using Spark" => spark.filterData()
    case _ => println("undefined command")
  }

}


object spark {

  case class sparkDetails(col_1 : String,
                          col_2 : String,
                          col_3 : String,
                          col_4 : String,
                          col_5 :String
                                  )

  def main(args: Array[String]): Unit = {

    val system = ActorSystem("akkActorSystem")
    val sparkActor = system.actorOf(Props[sparkActor], name = "sparkActor")

    sparkActor ! "buenos dias"
    sparkActor ! "filter data using Spark"

    system.terminate()
  }


  def filterData(): Unit ={
    val spark = SparkSession
      .builder()
      .master("local")
      .appName("AkkaAndSpark Application")
      .getOrCreate()

    /** For implicit conversions like converting RDDs to DataFrames* */
    import spark.implicits._


    val filePath = "/home/file/path/to/input.txt"
    val fileDF = spark.read.textFile(filePath)

    val outputRecord = fileDF
      .map(x => x.split("\\|",-1))
      .filter(r => r(0) == "spark")
      .map(r =>
        sparkDetails(
          r(0),
          r(1),
          r(2),
          r(3),
          r(4)
        ))

    outputRecord.show()
    outputRecord.write.mode("overwrite").csv("/home/file/path/to/output/")

    spark.stop()

  }
}
