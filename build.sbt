name := "akka"

version :="1.0.0"

scalaVersion :="2.12.7"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.12",
  "org.apache.spark"%%"spark-sql"%"2.4.0"
)
